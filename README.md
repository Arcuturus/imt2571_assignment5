# README #

## IMT2571 Assignment 5 ##
This repository contains the project files for assignment 5. 
### Getting started ###

Clone the project into folder of choice.

Please run sl.sql before running the php script:
```bash
source <full_file_path>/sl.sql
```

Then configure dbparams.php so it works with your DB settings:
```php
const DB_USER    = '';
const DB_PWD     = '';
const DB_HOST    = '';
const DB_NAME    = 'sl';
const TABLE_NAME = 'sl';
```
After the configuration you can run
```bash
php index.php
```

And you will get something like this:
![alt text](https://bytebucket.org/Arcuturus/imt2571_assignment5/raw/96b4cb2e582556c6b359282a8cfafdb561fedacd/skier_project/img/terminal_scap.png "Terminal output")
