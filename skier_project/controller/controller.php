<?php

include_once('model/club.php');
include_once('model/skier.php');
include_once('model/log.php');
include_once('model/season.php');
include_once('model/Model.php');

class Controller
{

  public $model;

  public function __construct()
  {
    $this->model = new Model();
  }

  public function invoke()
  {
    $this->model->run();
  }
}

 ?>
