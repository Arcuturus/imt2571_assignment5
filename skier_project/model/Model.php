<?php

include_once('club.php');
include_once('skier.php');
include_once('log.php');
include_once('entry.php');
include_once('season.php');
include_once('slModel.php');
include_once('dbparams.php');

class Model implements slModel
{

  public $clubsArray   = array();
  public $skiersArray  = array();
  public $seasonsArray = array();
  public $logsArray    = array();
  public $entriesArray = array();

  public $xml_file, $db;

  public function __construct()
  {
    $this->xml_file = simplexml_load_file("skierLogs.xml") or die("\nError: Could not fetch file\n\n");
    if($this->xml_file)
    {
      print("Fetched XML document\n");
    }
  }

  public function run() {
    $this->initDB();

    $this->fetchClubFromXML();
    $this->addClub();

    $this->fetchSkierfromXML();
    $this->addSkier();

    $this->fetchSeasonFromXML();
    $this->addSeason();

    $this->fetchLogFromXML();
    $this->addLog();
    $this->addEntry();
  }

  public function initDB()
  {
    $this->db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PWD);
    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    print("Connected to DB\n");
  }

  public function fetchClubFromXML()
  {
    $clubs  = $this->xml_file->xpath("//Clubs/Club");
    for($i = 0; $i < sizeof($clubs); $i++)
    {
      $id     = ($clubs[$i]['id'] . "");
      $name   = ($clubs[$i]->Name . "");
      $city   = ($clubs[$i]->City . "");
      $county = ($clubs[$i]->County . "");

      array_push($this->clubsArray, new Club($id, $name, $city, $county));
    }
    print("Fetched Clubs!\n");
  }

  public function addClub()
  {
    print("Adding clubs to DB...");
    foreach($this->clubsArray as $club)
    {
      $pstmt = $this->db->prepare
      (
        "INSERT INTO club (id, name, city, county)
         VALUES (?, ?, ?, ?)"
      );
      try
      {
        $pstmt->execute(array($club->id, $club->name, $club->city, $club->county));
      }
      catch (PDOException $pdoe)
      {
        print($pdoe->getMessage);
        break;
      }
    }
    print("   Added!\n");
  }

  public function fetchSkierFromXML()
  {
    $skiers = $this->xml_file->xpath("//Skier");
    $exists = false;

    for($i = 0; $i < sizeof($skiers); $i++)
    {
      $userName    = ($skiers[$i]['userName'] . "");
      $firstName   = ($skiers[$i]->FirstName . "");
      $lastName    = ($skiers[$i]->LastName . "");
      $yearOfBirth = ($skiers[$i]->YearOfBirth . "");

      foreach($this->skiersArray as $skier)
      {
        // Check if userName is already registered in array
        if($skier->userName == $userName)
        {
          // Found match
          $exists = true;
        }
      }
      // If no match, add the Skier to array
      if(!$exists)
      {
        array_push($this->skiersArray, new Skier($userName, $firstName, $lastName, $yearOfBirth));
      }
    }
    print("Fetched Skiers!\n");
  }

  public function addSkier()
  {
    print("Adding skiers to DB...");
    foreach($this->skiersArray as $skier)
    {
      $pstmt = $this->db->prepare
      (
        "INSERT INTO skier (userName, firstName, lastName, yearOfBirth)
         VALUES (?, ?, ?, ?)"
      );
      try
      {
        $pstmt->execute(array(
          $skier->userName,
          $skier->firstName,
          $skier->lastName,
          $skier->yearOfBirth));
      }
      catch (PDOException $pdoe)
      {
        print($pdoe->getMessage);
        break;
      }
    }
    print("   Added!\n");
  }

  public function fetchLogFromXML()
  {
    $logId = 0;
    $data = $this->xml_file->xpath("//Season//Skier/Log/Entry//ancestor::Season");
    foreach($data as $fallYear)
    {
      foreach($fallYear as $club)
      {
        foreach($club as $skier)
        {
          foreach($skier as $log)
          {
            $logId++;
            $lFallYear = ($fallYear['fallYear'] . "");
            $lUserName = ($skier['userName'] . "");
            $leid    = $logId;
            $totDist = $this->xml_file->xpath("//Season[@fallYear = '" .$lFallYear. "']
                                                   //Skier[@userName = '" .$lUserName. "']
                                                   /Log/Entry/Distance");

            $dist = 0;
            foreach($totDist as $dis) // sum does not work, have to do it manually
            {
              $dist += $dis;
            }

            array_push($this->logsArray, new Log($lFallYear, $lUserName, $leid, $dist));

            foreach($log as $entry)
            {
              $eid      = $logId;
              $logDate  = ($entry->Date . "");
              $area     = ($entry->Area . "");
              $distance = ($entry->Distance . "");
              array_push($this->entriesArray, new Entry($eid, $logDate, $area, $distance));
            }
          }
        }
      }
    }
    print("Fetched Logs and Entries!\n");
  }

  public function addLog()
  {
    print("Adding logs to DB...");
    foreach($this->logsArray as $log)
    {
      $pstmt = $this->db->prepare
      (
        "INSERT INTO log (fallYear, userName, eid, totalDistance)
         VALUES (?, ?, ?, ?)"
      );
      try
      {
        $pstmt->execute(array(
          $log->fallYear,
          $log->userName,
          $log->eid,
          $log->totalDistance
        ));
      }
      catch (PDOException $pdoe)
      {
        print($pdoe->getMessage());
        break;
      }
    }
    print("   Added!\n");
    print("\nCleaning up....\n");
  }

  public function addEntry()
  {
    foreach($this->entriesArray as $entry)
    {
      $pstmt = $this->db->prepare
      (
        "INSERT INTO entry (eid, logDate, area, distance)
         VALUES (?, ?, ?, ?)"
      );
      try
      {
        $pstmt->execute(array(
          $entry->eid,
          $entry->logDate,
          $entry->area,
          $entry->distance
        ));
      }
      catch (PDOException $pdoe)
      {
        print($pdoe->getMessage());
        break;
      }
    }
  }

  public function fetchSeasonFromXML()
  {
    $x = $this->xml_file;
    $seasons = $x->xpath("//Season");
    for($i = 0; $i < sizeof($seasons); $i++)
    {
      $clubs = $seasons[$i]->Skiers;
      for($j = 0; $j < sizeof($clubs); $j++)
      {
        $skiers = $clubs[$j]->Skier;
        for($k = 0; $k < sizeof($skiers); $k++)
        {
          $season = ($seasons[$i]['fallYear'] . "");
          $club = ($clubs[$j]['clubId'] . "");
          $skier = ($skiers[$k]['userName'] . "");
          array_push($this->seasonsArray, new Season($season, $club, $skier));
        }
      }
    }
    print("Fetched Seasons!\n");
  }

  public function addSeason()
  {
    print("Adding seasons to DB...");
    foreach($this->seasonsArray as $season)
    {
      $pstmt = $this->db->prepare
      (
        "INSERT INTO season (fallYear, clubId, userName)
         VALUES (?, ?, ?)"
      );
      try
      {
        if($season->clubId == "")
        {
          $clubs =
          $pstmt->execute(array(
            $season->fallYear,
            null,
            $season->userName
          ));
        }
        else
        {
          $clubs =
          $pstmt->execute(array(
            $season->fallYear,
            $season->clubId,
            $season->userName
          ));
        }
      }
      catch (PDOException $pdoe)
      {
        print($pdoe->getMessage());
        break;
      }
    }
    print("   Added!\n");
  }
}

?>
