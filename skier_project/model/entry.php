<?php

class Entry
{
  public $eid;
  public $logDate;
  public $area;
  public $distance;

  public function __construct($eid, $logDate, $area, $distance)
  {
    $this->eid      = $eid;
    $this->logDate  = $logDate;
    $this->area     = $area;
    $this->distance = $distance;
  }
}

?>
