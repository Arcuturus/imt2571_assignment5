<?php

class log
{
  public $fallYear;        // FK, CPK
  public $userName;        // FK, CPK
  public $eid;             // FK
  public $totalDistance;

  public function __construct($fallYear, $userName, $eid, $totalDistance)
  {
    $this->fallYear      = $fallYear;
    $this->userName      = $userName;
    $this->eid           = $eid;
    $this->totalDistance = $totalDistance;
  }

}

?>
