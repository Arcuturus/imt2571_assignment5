<?php

class Season {

  public $fallYear;
  public $clubId;
  public $userName;

  public function __construct($fallYear, $clubId, $userName)
  {
    $this->fallYear = $fallYear;
    $this->clubId   = $clubId;
    $this->userName = $userName;
  }

}

?>
