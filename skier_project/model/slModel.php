<?php

Interface slModel {

  public function fetchClubFromXML();
  public function addClub();

  public function fetchSkierFromXML();
  public function addSkier();

  public function fetchLogFromXML();
  public function addLog();

  public function addEntry();

  public function fetchSeasonFromXML();
  public function addSeason();

}

?>
