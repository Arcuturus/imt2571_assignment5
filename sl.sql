DROP SCHEMA IF EXISTS sl;
CREATE SCHEMA sl;

USE sl;

CREATE TABLE skier (
	userName    VARCHAR(60) NOT NULL PRIMARY KEY,
    firstName   VARCHAR(60) NOT NULL,
    lastName    VARCHAR(60) NOT NULL,
    yearOfBirth VARCHAR(4)  NOT NULL
) CHARACTER SET latin1 COLLATE latin1_danish_ci;

CREATE TABLE club (
	id     VARCHAR(60) PRIMARY KEY,
    name   VARCHAR(60) NOT NULL,
    city   VARCHAR(60) NOT NULL,
    county VARCHAR(20) NOT NULL
) CHARACTER SET latin1 COLLATE latin1_danish_ci;

CREATE TABLE season (
	fallYear VARCHAR(4) NOT NULL,
    clubId   VARCHAR(60),
    userName VARCHAR(60) NOT NULL,
    PRIMARY KEY(fallYear, userName)
) CHARACTER SET latin1 COLLATE latin1_danish_ci;

CREATE TABLE log (
	fallYear      VARCHAR(4) NOT NULL,
    userName      VARCHAR(60) NOT NULL,
    eid           INT,
    totalDistance INT
) CHARACTER SET latin1 COLLATE latin1_danish_ci;

CREATE TABLE entry (
	eid INT,
    logDate DATE NOT NULL,
    area VARCHAR(60) NOT NULL,
    distance INT,
    PRIMARY KEY(eid, logDate)
) CHARACTER SET latin1 COLLATE latin1_danish_ci;

ALTER TABLE season
ADD FOREIGN KEY (clubId) REFERENCES club(id);

ALTER TABLE season
ADD FOREIGN KEY (userName) REFERENCES skier(userName);

ALTER TABLE log
ADD FOREIGN KEY (userName) REFERENCES skier(userName);

#ALTER TABLE log
#ADD FOREIGN KEY (eid) REFERENCES entry(eid);

ALTER TABLE log
ADD FOREIGN KEY (fallYear) REFERENCES season(fallYear);
